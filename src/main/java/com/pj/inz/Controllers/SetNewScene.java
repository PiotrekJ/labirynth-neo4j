package com.pj.inz.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;

class SetNewScene {
    private FXMLLoader loader;
    public SetNewScene(String fxmlFile, ActionEvent event) throws Exception{
        loader = new FXMLLoader(this.getClass().getResource(fxmlFile));
        ((Node)event.getSource()).getScene().setRoot((Parent)loader.load());
    }

    public FXMLLoader getLoader()
    {
        return loader;
    }
}
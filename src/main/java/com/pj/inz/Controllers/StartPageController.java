package com.pj.inz.Controllers;


import com.pj.inz.Model.GraphSolver;
import com.pj.inz.Neo4jSessionFactory;
import com.pj.inz.Model.Square;

import com.pj.inz.Model.entities.GraphNode;
import com.pj.inz.Model.entities.Labirynth;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;
import org.neo4j.ogm.session.Session;

import java.util.*;

public class StartPageController
{
    public ListView<Labirynth> listView;
    public Canvas labirynthCanvas;
    Square fields[][];
    int STEP=20;
    GraphicsContext gc;
    Session session;
    public void initialize(){
        gc = labirynthCanvas.getGraphicsContext2D();
        session= Neo4jSessionFactory.getInstance().getNeo4jSession();
        Collection<Labirynth> graphList = session.loadAll(Labirynth.class);
        ObservableList<Labirynth> ol = FXCollections.observableArrayList(graphList);
        listView.setCellFactory(param -> new ListCell<Labirynth>() {
            @Override
            protected void updateItem(Labirynth item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null || item.getName() == null) {
                    setText(null);
                } else {
                    setText(item.getName());
                }
            }
        });
        listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Labirynth>() {
            @Override
            public void changed(ObservableValue<? extends Labirynth> observable, Labirynth oldValue, Labirynth newValue) {
                drawLabirynth(newValue,gc);
            }
        });

        listView.setItems(ol);
        for (Labirynth l :
                graphList) {
            System.out.println(l.getId());
        }



        Labirynth[] graphArray = graphList.toArray(new Labirynth[graphList.size()]);
        if(graphArray.length>0) {
            drawLabirynth(graphArray[0],gc);
        }

        }

        void drawLabirynth(Labirynth labirynth, GraphicsContext gc){
            int sizeX= labirynth.getSizeX();
            int sizeY= labirynth.getSizeY();
            labirynthCanvas.setHeight(sizeY*STEP);
            labirynthCanvas.setWidth(sizeX *STEP);
            fields=new Square[sizeX][sizeY];
            for(int i=0; i<sizeX; i++){
                for(int j=0; j<sizeY;j++) {
                    Square square = new Square(i,j,i * STEP, j*STEP, STEP);
                    fields[i][j]= square;
                    square.draw(gc);
                }
            }
            if(labirynth.getStart()!=null)
                fields[labirynth.getStart().getX()][labirynth.getStart().getY()].fill(gc, Color.YELLOW);
            if(labirynth.getFinish()!=null)
                fields[labirynth.getFinish().getX()][labirynth.getFinish().getY()].fill(gc, Color.WHITE);
            for (GraphNode tile:
                 labirynth.nodes) {
                    fields[tile.getX()][tile.getY()].fill(gc, Color.GREEN);
                System.out.println(tile.getX() + " " + tile.getY());
            }
        }
    public void newLabirynthButton(ActionEvent actionEvent) {
        try {
            new SetNewScene("/fxml/labirynthCreator.fxml", actionEvent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void solveButtonAction(ActionEvent actionEvent) {
            solveMaze();
    }

    void solveMaze(){
        if(listView.getSelectionModel().getSelectedItems().size()==0) {
            listView.getSelectionModel().select(0);
            listView.getFocusModel().focus(0);
        }
        Labirynth labirynth = listView.getSelectionModel().getSelectedItem();
        GraphSolver solver=new GraphSolver(session,fields,gc);
        LinkedList<GraphNode> result = solver.knightsTour(labirynth.getStart(),labirynth.getFinish(), new HashSet<GraphNode>());
        result.forEach(x-> fields[x.getX()][x.getY()].fill(gc, Color.RED));
//        String q= "MATCH (n1:NODE),(n2:NODE), p = shortestPath((n1)-[:CONNECTION*]-(n2))" +
//                " WHERE ID(n1)="+ labirynth.getStart().getId() +" and ID(n2)=" + labirynth.getFinish().getId() +
//                " RETURN p";
//        HashMap<String, GraphNode> myMap= new HashMap<>();
//        Result res = session.query(q,myMap);
//        System.out.println(res.queryResults());
    }
}



package com.pj.inz.Controllers;

//piotr@NASA2u:~/Pulpit/jfx/labirynth$ mvn clean compile assembly:single

import com.pj.inz.Model.GraphCreator;
import com.pj.inz.Model.Square;
import javafx.event.ActionEvent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import org.neo4j.driver.v1.*;

public class LabirynthCreatorController
{
    public TextField nameTextField;

    public void onMouseMoved(MouseEvent mouseEvent) {
//        System.out.println((int)(mouseEvent.getX()-START_X)/SIZE_X);
        //System.out.println((mouseEvent.getX()-START_X)/SIZE_X);
        for(int i=0; i<SIZE_X; i++){
            for(int j=0; j<SIZE_Y;j++) {
                if(fields[i][j].inRange((int)mouseEvent.getX(),(int)mouseEvent.getY())
                        && !fields[i][j].isStart() && !fields[i][j].isFinish()) {
                    fields[i][j].fill(gc, Color.GREEN);
                    if(CurrentlyHihlighted.x>-1 && (CurrentlyHihlighted.x!=i || CurrentlyHihlighted.y!=j)) {
                        if(!fields[CurrentlyHihlighted.x][CurrentlyHihlighted.y].isSelected())
                            fields[CurrentlyHihlighted.x][CurrentlyHihlighted.y].unfill(gc);
                    }
                    if(CurrentlyHihlighted.x!=i || CurrentlyHihlighted.y!=j) {
                        CurrentlyHihlighted.x = i;
                        CurrentlyHihlighted.y = j;
                    }
                }
            }
        }
    }
    public void onMouseClicked(MouseEvent mouseEvent) {
        //System.out.println(mouseEvent.getX() + " " +(int)mouseEvent.getY());
        for(int i=0; i<SIZE_X; i++){
            for(int j=0; j<SIZE_Y;j++) {
                if(fields[i][j].inRange((int)mouseEvent.getX(),(int)mouseEvent.getY())) {
                    if(!fields[i][j].isSelected()) {
                        fields[i][j].select();
                        //System.out.println(fields[0]);
                    }
                    else {
                        if(!settingStart & !settingFinish)
                            fields[i][j].unselect();
                    }
                    if(settingStart) {
                        if(!fields[i][j].isStart())
                            fields[i][j].setStart(true, gc);
                        else
                            fields[i][j].setStart(false, gc);
                        settingStart=false;
                    }
                    if(settingFinish) {
                        if(!fields[i][j].isFinish())
                            fields[i][j].setFinish(true, gc);
                        else
                            fields[i][j].setFinish(false, gc);
                        settingFinish=false;
                    }
                }
            }
        }
    }
    public void makeGraphButton(ActionEvent actionEvent) {
        GraphCreator.makeGraph(fields,nameTextField.getText(),SIZE_X, SIZE_Y);
    }


    public void setStartButton(ActionEvent actionEvent) {
        if(!settingStart)
            settingStart=true;
        else
            settingStart=false;
    }

    boolean settingStart;
    boolean settingFinish;
    private static final Logger log = LoggerFactory.getLogger(LabirynthCreatorController.class);
    public Canvas labirynthCanvas;
    int STEP=20;
    int SIZE_X=20;
    int SIZE_Y=20;
    int START_X=20;
    int START_Y=20;

    public void setFinishButton(ActionEvent actionEvent) {
        if(!settingFinish)
            settingFinish=true;
        else
            settingFinish=false;
    }


    public void backButtonAction(ActionEvent actionEvent) {
        try {
            new SetNewScene("/fxml/startPage.fxml", actionEvent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class CurrentlyHihlighted{
        public static int x=-1;
        public static int y=-1;
    }
    Square fields[][];
    GraphicsContext gc;
    public void initialize(){
        gc = labirynthCanvas.getGraphicsContext2D();
        labirynthCanvas.setHeight(SIZE_Y*STEP+START_Y);
        labirynthCanvas.setWidth(SIZE_X *STEP+START_X);
        fields=new Square[SIZE_X][SIZE_Y];
        for(int i=0; i<SIZE_X; i++){
            for(int j=0; j<SIZE_Y;j++) {
                Square square = new Square(i,j,START_X + i * STEP, START_Y+j*STEP, STEP);
                fields[i][j]= square;
                square.draw(gc);
                System.out.println(i);
            }
        }
    }

}

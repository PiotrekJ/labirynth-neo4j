package com.pj.inz;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MainApp extends Application {

    private static final Logger log = LoggerFactory.getLogger(MainApp.class);

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    public void start(Stage stage) throws Exception {

//        Driver driver = GraphDatabase.driver( "bolt://localhost:7687",
//                AuthTokens.basic( "neo4j", "admin123" ) );
//        Session session = driver.session();
//
//        session.run( "CREATE (a:Person {name: {name}, title: {title}})",
//                parameters( "name", "Arthur", "title", "King" ) );
//
//        session.run( "CREATE (a:Person {name: {name}, title: {title}})",
//                parameters( "name", "Piotr", "title", "Dev" ) );
//
//        StatementResult result = session.run( "MATCH (a:Person) WHERE a.name = {name} " +
//                        "RETURN a.name AS name, a.title AS title",
//                parameters( "name", "Piotr" ) );
//        while ( result.hasNext() )
//        {
//            Record record = result.next();
//            System.out.println( record.get( "title" ).asString() + " " + record.get( "name" ).asString() );
//        }
//
//        session.close();
//        driver.close();

        //----------------------------------------
        log.info("Starting Hello JavaFX and Maven demonstration application");

        String fxmlFile = "/fxml/startPage.fxml";
        log.debug("Loading FXML for main view from: {}", fxmlFile);
        FXMLLoader loader = new FXMLLoader();
        Parent rootNode = (Parent) loader.load(getClass().getResourceAsStream(fxmlFile));

        log.debug("Showing JFX scene");
        Scene scene = new Scene(rootNode, 800, 600);
        scene.getStylesheets().add("/styles/styles.css");

        stage.setTitle("Hello JavaFX and Maven");
        stage.setScene(scene);
        stage.show();
    }
}

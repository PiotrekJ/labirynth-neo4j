package com.pj.inz.Model;

import com.pj.inz.Model.entities.GraphNode;
import com.pj.inz.Model.entities.Labirynth;
import com.pj.inz.Neo4jSessionFactory;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;

/**
 * Created by piotr on 08.01.18.
 */
public class GraphCreator {
    static public void makeGraph(Square[][] fields, String name, int SIZE_X, int SIZE_Y){
        Session session = Neo4jSessionFactory.getInstance().getNeo4jSession();
        Transaction tx = session.beginTransaction();
        try{
            Labirynth labirynth = new Labirynth(SIZE_X, SIZE_Y);
            labirynth.setName(name); //nameTextField.getText()
            for (int row = 0; row < SIZE_Y; row++) {
                for (int col = 0; col < SIZE_X; col++) {
                    if (fields[col][row].isSelected()) {
                        GraphNode n;
                        if (!fields[col][row].isNode()) {
                            n = new GraphNode(col, row);
                            fields[col][row].setNode(n);
                            if (fields[col][row].isStart()) {
                                labirynth.setStart(fields[col][row].getNode());
                            } else if (fields[col][row].isFinish()) {
                                labirynth.setFinish(fields[col][row].getNode());
                            } else
                                labirynth.nodes.add(n);
                        } else
                            n = fields[col][row].getNode();

                        for (Square s :
                                fields[col][row].getNeighbours(fields, SIZE_X, SIZE_Y)) {
                            if (s.isNode()) {
                                n.connections.add(s.getNode());
                            } else {
                                GraphNode newNode = new GraphNode(s.getCol(), s.getRow());
                                if (s.isStart()) labirynth.setStart(newNode);
                                else if (s.isFinish()) labirynth.setFinish(newNode);
                                else {
                                    labirynth.nodes.add(newNode);
                                }
                                s.setNode(newNode);
                                session.save(newNode);
                                n.connections.add(newNode);
                            }
                        }
                        session.save(n);
                    }
                }
            }
            session.save(labirynth, 1);
            tx.commit();
        }
        finally {
            tx.close();
        }
    }
}

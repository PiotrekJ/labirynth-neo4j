package com.pj.inz.Model;

import com.pj.inz.Model.entities.GraphNode;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import org.neo4j.ogm.model.Result;
import org.neo4j.ogm.session.Session;

import java.util.*;

/**
 * Created by piotr on 06.01.18.
 */
public class GraphSolver {
    public GraphSolver(Session session, Square[][] fields, GraphicsContext gc) {
        this.session = session;
        this.fields = fields;
        //this.gc = gc;
    }

    Session session;
    Square fields[][];
    //GraphicsContext gc;
    HashMap<String, GraphNode> myMap= new HashMap<>();

    public LinkedList<GraphNode> knightsTour(GraphNode node, GraphNode finish, HashSet<GraphNode> visited){
        visited.add(node);
        HashSet<GraphNode> newVisited = new HashSet<GraphNode>(visited);

        String q= "MATCH(a:NODE)-[:CONNECTION]->(b:NODE) WHERE ID(a)="+ node.getId() + " RETURN b";
        Result res = session.query(q,myMap);
        ArrayList<GraphNode> relations = new ArrayList<GraphNode>();
        res.queryResults().forEach(x-> relations.add((GraphNode)x.values().toArray()[0]));

        if(node==finish) {
            LinkedList<GraphNode> path = new LinkedList<GraphNode>();
            path.add(node);
            return path;
        }
        if(relations.size()>2) {
            HashSet<LinkedList<GraphNode>> pathsSet = new HashSet<LinkedList<GraphNode>>();
            for (GraphNode neighbour :
                    relations) {
                if (!visited.contains(neighbour)) {
                    LinkedList<GraphNode> currPath = knightsTour(neighbour, finish, newVisited);
                    if (currPath != null) {
                        currPath.add(node);
                        pathsSet.add(currPath);
                    }
                }
            }


            if (pathsSet.isEmpty())
                return null;
            return pathsSet.stream().min((p1, p2) -> Integer.compare(p1.size(), p2.size())).get();
        }
        else{
            for (GraphNode neighbour :
                    relations) {
                if (!visited.contains(neighbour)) {
                    LinkedList<GraphNode> currPath = knightsTour(neighbour, finish, newVisited);
                    if (currPath != null) {
                        currPath.add(node);
                        return currPath;
                    }
                }
            }
        }
        return null;
    }

}

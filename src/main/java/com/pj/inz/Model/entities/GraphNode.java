package com.pj.inz.Model.entities;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;

/**
 * Created by piotr on 28.11.17.
 */
@NodeEntity(label="NODE")
public class GraphNode extends MyEntity {

    @Override
    public Long getId() {
        return id;
    }

    @Id
    @GeneratedValue
    public Long id;

    int x,y;



    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public GraphNode(){}

    public GraphNode(int x, int y)
    {
        System.out.println("New node" + x + ", " + y);
        this.x=x;
        this.y=y;
        connections= new HashSet<GraphNode>();
    }

    public HashSet<GraphNode> getConnections() {
        return connections;
    }

    @Relationship(type = "CONNECTION")
    public HashSet<GraphNode> connections;

}

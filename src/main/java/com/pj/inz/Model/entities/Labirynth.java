package com.pj.inz.Model.entities;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

import java.util.HashSet;

/**
 * Created by piotr on 28.11.17.
 */
@NodeEntity(label="LABIRYNTH")
public class Labirynth {

    @Id
    @GeneratedValue
    public Long id;

    String name;
    int sizeX;

    public int getSizeX() {
        return sizeX;
    }

    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }

    int sizeY;
    public Labirynth(){
        nodes=new HashSet<GraphNode>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GraphNode getStart() {

        return start;
    }

    public void setStart(GraphNode start) {
        this.start = start;
    }

    public GraphNode getFinish() {
        return finish;
    }

    public void setFinish(GraphNode finish) {
        this.finish = finish;
    }

    public Labirynth(int sizeX, int sizeY)
    {
        setSizeX(sizeX);
        setSizeY(sizeY);
        this.name=name;
        nodes= new HashSet<GraphNode>();
    }

    private GraphNode start;
    private GraphNode finish;

    //@Relationship(type = "CONNECTION")
    public HashSet<GraphNode> nodes;

}

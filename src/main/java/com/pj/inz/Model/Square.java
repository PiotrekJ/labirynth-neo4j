package com.pj.inz.Model;

import com.pj.inz.Model.entities.GraphNode;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;

/**
 * Created by piotr on 30.11.17.
 */
public class Square
{
    private boolean selected;
    private boolean isNode;
    private GraphNode node;
    int x_draw,y_draw, size;
    int row;int col;

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    private boolean start, finish;

    public boolean isStart() {
        return start;
    }

    public void setStart(boolean start, GraphicsContext gc) {
        this.start = start;
        if(start){
            gc.setFill(Color.YELLOW);
            gc.fillRect(x_draw,y_draw,size,size);
        }
        else
            unfill(gc);
    }


    public Square(int col, int row, int x, int y, int size){
        this.row=row;
        this.col=col;
        isNode=false;
        selected=false;
        this.x_draw=x;
        this.y_draw=y;
        this.size=size;
    }

    public ArrayList<Square> getNeighbours(Square fields[][], int size_x, int size_y){
        ArrayList<Square> res=new ArrayList<Square>();
        if(col>0)
            if(fields[col-1][row].isSelected()) res.add(fields[col-1][row]);
        if(col<size_x-1)
            if(fields[col+1][row].isSelected()) res.add(fields[col+1][row]);
        if(row>0)
            if(fields[col][row-1].isSelected()) res.add(fields[col][row-1]);
        if(row<size_y-1)
            if(fields[col][row+1].isSelected()) res.add(fields[col][row+1]);
        return res;
    }

    public boolean isNode() {
        return isNode;
    }
    public GraphNode getNode() {
        return node;
    }
    public void setNode(GraphNode node) {
        this.node = node;
        isNode = true;
    }
    public void select(){
        selected=true;
    }
    public void unselect(){
        selected=false;
    }
    public boolean isSelected(){
        return selected;
    }
    public boolean inRange(int x_pos, int y_pos){
        if(x_pos>x_draw && x_pos<x_draw+size){
            if(y_pos>y_draw && y_pos<y_draw+size){
                return true;
            }
        }
        return false;
    }
    public void fill(GraphicsContext gc, Color color){
        gc.setFill(color);
        gc.fillRect(x_draw,y_draw,size,size);
    }
    public void unfill(GraphicsContext gc){
        gc.setFill(Color.GREY);
        gc.fillRect(x_draw,y_draw,size,size);
        gc.strokeRect(x_draw,y_draw,size,size);
        gc.restore();
    }
    public void draw(GraphicsContext gc){
        gc.setStroke(Color.BLUE);
        gc.setFill(Color.GREY);
        gc.fillRect(x_draw,y_draw,size,size);
        gc.strokeRect(x_draw,y_draw,size,size);
    }

    public boolean isFinish() {
        return finish;
    }

    public void setFinish(boolean finish, GraphicsContext gc) {
        if(finish){
            gc.setFill(Color.WHITE);
            gc.fillRect(x_draw,y_draw,size,size);
        }
        else
            unfill(gc);
        this.finish = finish;
    }
}